<?php
  
  // Parent class
  abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahkaki;
    public $keahlian;
  
    public function __construct($name) {
      $this->nama = $name;
    }

    abstract public function atraksi();
  }

  abstract class Fight extends Hewan {
    
    public $attackPower;
    public $defencePower;
   
     abstract public function serang();
     abstract public function diserang();
  }

  class Elang extends Hewan{

    public $jumlahkaki = 2;
    public $keahlian = "Terbang Tinggi";
    

    public function atraksi() : string {
       return "$this->nama $this->keahlian" ;
    }
  }
  
  $elang = new Elang("Elang");
  echo "Jenis Hewan : " . $elang -> nama . "<br>";
  echo "Jumlah Kaki : " . $elang -> jumlahkaki . "<br>";
  echo "Keahlian : " . $elang -> keahlian . "<br>";
  echo "Atraksi : " . $elang ->atraksi() . "<br> <br>";

  class Harimau extends Hewan{

    public $jumlahkaki = 4;
    public $keahlian = "Lari Cepat";
    

    public function atraksi() : string {
       return "$this->nama $this->keahlian" ;
    }
  }
  
  $harimau = new Harimau("Harimau");
  echo "Jenis Hewan : " . $harimau -> nama . "<br>";
  echo "Jumlah Kaki : " . $harimau -> jumlahkaki . "<br>";
  echo "Keahlian : " . $harimau -> keahlian . "<br>";
  echo $harimau->atraksi();


  
  


?>